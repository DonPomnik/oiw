﻿var myHeading = document.querySelector('h1');
myHeading.textContent = 'Hejo!';

/*
krótki komentarz 
*/

var myImage = document.querySelector('img');

myImage.onclick = function() {
    var mySrc = myImage.getAttribute('src');
    if(mySrc === 'images/puppy.jpg') {
      myImage.setAttribute ('src','images/puppy2.jpg');
    } else {
      myImage.setAttribute ('src','images/puppy.jpg');
    }
}
function funkcjaX() {
  document.getElementById("demo").innerHTML = "Wyświetlono funkcje X";
}

function funkcjaY() {
  document.getElementById("demo").innerHTML = "Wyświetlono funkcje Y";
}

var myButton = document.querySelector('button');
var myHeading = document.querySelector('h1');

function wpisywanie() {
  var myName = prompt('Podaj swoje imie: ');
  localStorage.setItem('name', myName);
  myHeading.textContent = 'Mozilla jest spoko, ' + myName;
}

if(!localStorage.getItem('name')) {
  	setUserName();
	}
  else {
  	var storedName = localStorage.getItem('name');
  	myHeading.textContent = 'Mozilla jest spoko, ' + storedName;
}

