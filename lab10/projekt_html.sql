-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 03 Lis 2018, 23:27
-- Wersja serwera: 10.1.33-MariaDB
-- Wersja PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projekt_html`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `artykuly`
--

CREATE TABLE `artykuly` (
  `id_a` int(11) NOT NULL,
  `head` char(200) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `main` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `artykuly`
--

INSERT INTO `artykuly` (`id_a`, `head`, `main`) VALUES
(1, 'Heroes of Might and Magic 3 na sterydach ? najlepsze mody do HoMM3', 'Czy komuś jeszcze zdarza się grać ze znajomymi na jednym komputerze? Seria Heroes of Might & Magic z nieśmiertelną „trójką” na czele zawsze będzie kojarzyć mi się z długimi wieczorami (albo i wagarami), w trakcie których wyrywaliśmy sobie myszkę i kombinowaliśmy na wszelkie sposoby, by zniszczyć kolegę ze szkolnej ławki. Ot, takie przyjacielskie posiedzenie.\r\n'),
(2, 'Assassin\'s Creed Odyssey z pierwszym darmowym DLC', 'Ubisoft poinformował, że najnowsza aktualizacja 1.0.6, poza licznymi poprawkami i usprawnieniami (m.in. opcją automatycznego tworzenia strzał i szczegółową kartą postaci – z pełną listą możecie zapoznać się tutaj, ale uważajcie na spoilery), dodała do gry Assassin’s Creed Odyssey fabularne rozszerzenie – część serii The Lost Tales of Greece. Jest ono dostępne na wszystkich platformach, zupełnie za darmo i dodaje do gry nową misję, zatytułowaną The Show Must Go On. Patch „waży” 4,1 GB na PlayStation 4 oraz Xboksie One i 3,5 GB na pecetach.\r\n'),
(3, 'BlizzCon 2018 ? zapowiedziano Warcraft 3: Reforged', 'ani uniwersum Warcraft wydali z siebie okrzyk radości, gdy podczas panelu otwierającego konwent BlizzCon 2018 studio Blizzard zapowiedziało grę Warcraft III: Reforged. Jest to długo oczekiwana odświeżona wersja gry Warcraft III z 2002 roku. Legendarny i klasyczny już RTS powróci do posiadaczy komputerów PC w zupełnie nowej szacie graficznej – także wraz z niezwykle dobrze przyjętym dodatkiem The Frozen Throne. Poza zmianami kosmetycznymi, twórcy zadbali o nowy balans rozgrywki, a także wsparcie dla edytora poziomów i gier, który był miejscem narodzin wielu interesujących projektów, w tym pierwszych produkcji typu MOBA.\r\n'),
(4, 'Recenzja gry Call of Cthulhu ? czy horror Lovecrafta straszy?', 'W dobrej klasycznej powieści detektywistycznej ta ostatnia, najtrudniejsza sprawa zawsze przychodzi sama do zmęczonego życiem, nadużywającego alkoholu detektywa. W Call of Cthulhu, najnowszej grze studia Cyanide, tak właśnie się dzieje. Edward Pierce, weteran wielkiej wojny, budzi się z koszmaru na kanapie we własnym biurze. Pierwszą myślą jest chęć sięgnięcia po szklaneczkę czegoś mocniejszego, by uspokoić skołatane nerwy. Być może gracz pozwoli bohaterowi skosztować trunku, być może nie, ale do drzwi i tak zapuka interesant.'),
(5, 'Mały telefon – jaki smartfon do 5 cali warto kupić?', 'Znalezienie smartfona wygodnie mieszczącego się w dłoni w dzisiejszych czasach stanowi spory kłopot. W odnalezieniu odpowiedniego modelu z pewnością pomoże Wam nasze zestawienie najciekawszych modeli z ekranem o przekątnej do 5 cali.\r\n'),
(6, 'Gry października 2018 – najciekawsze premiery i konkurs', 'W tym miesiącu król będzie jeden. Po wielu latach oczekiwań Rockstar Games w końcu pozwoli nam wrócić na Dziki Zachód w kontynuacji fenomenalnego Red Dead Redemption. Gra zapowiada się kapitalnie, renoma firmy ją tworzącej jest niepodważalna – ciężko sobie wyobrazić katastrofę, która spowodowałaby, że coś poszłoby nie tak. RDR2 nie będzie jedyną atrakcją października. Zagramy także w kolejne odsłony Assassin’s Creed, Call of Duty i Soulcalibura (z świetnie zapowiadającym się gościnnym występem Geralta z Rivii), wypróbujemy singleplayerową kampanię do Gwinta, która wyewoluowała w samodzielną grę, oraz sprawdzimy ciekawe polskie projekty, jak World War 3, Kursk i My Memory of Us. Na nudę narzekać nie będziemy!\r\n'),
(7, 'BlizzCon 2018 – Destiny 2 na PC za darmo', 'Konwent BlizzCon stał się miejscem, który odwiedzają nie tylko pracownicy studia Blizzard, ale także producenci związani z firmą Activision, w tym studio Bungie, pracujące od lat nad serią Destiny. Przedstawiciele dewelopera zapowiedzieli podczas imprezy, że z okazji pierwszej rocznicy premiery Destiny 2 na pecetach, podstawowa wersja gry zostanie udostępniona za darmo wszystkim chętnym w dniach 2-18 listopada. Grę pobierzemy jedynie przy pomocy klienta Battle.net. Warto zaznaczyć, że po zakończeniu promocji tytuł pozostanie w naszej cyfrowej bibliotece\r\nŹródło: https://www.gry-online.pl/S013.asp?ID=111868'),
(11, 'BlizzCon 2018 – zapowiedziano Diablo Immortal', 'Przez dłuższy czas fani Blizzarda liczyli po cichu, że na tegorocznym konwencie BlizzCon usłyszą wreszcie jakieś wieści na temat czwartej odsłony Diablo. Taki obrót spraw miały sugerować liczne przecieki oraz krążące po sieci plotki. Deweloperzy oczywiście zaprzeczali wszystkim spekulacjom. Jak się jednak dziś okazało, nie były one całkowicie błędne. Podczas panelu otwierającego imprezę, Blizzard rzeczywiście zapowiedział nową grę z serii pod tytułem Diablo Immortal. Stworzona została ona jednak wyłącznie z myślą o urządzeniach mobilnych wyposażonych w system iOS lub Android i pełni rolę fabularnego pomostu łączącego drugą oraz trzecią część cyklu. Premiera planowana jest na przyszły rok. Tytuł najprawdopodobniej funkcjonować będzie w modelu free-to-play.\r\nŹródło: https://www.gry-online.pl/S013.asp?ID=111870'),
(12, 'Hitman Absolution i Hitman Blood Money trafią na Xboksa One i PS4?', 'Fani Agenta 47 odliczają dni do premiery Hitmana 2. Wkrótce mogą także otrzymać okazję do przypomnienia sobie wcześniejszych odsłon cyklu, gdyż wiele wskazuje na to, że na PlayStation 4 i Xboksa One zmierzają gry Hitman: Absolution oraz Hitman: Blood Money. Źródłem tych doniesień jest baza danych grupy PEGI. Oba tytuły zostały dodane w sierpniu, ale dopiero teraz zostały zauważone przez serwis Gematsu. Organizacje zajmujące się przydzielaniem kategorii wiekowych są od dawna doskonałym źródłem przecieków o niezapowiedzianych jeszcze oficjalnie projektach i najczęściej informacje te znajdują pokrycie w rzeczywistości.\r\nŹródło: https://www.gry-online.pl/S013.asp?ID=111873'),
(13, 'Autor Metro 2033 o Andrzeju Sapkowskim: \"niewdzięczny palant\"', 'W tym miesiącu jedną ze spraw budzących największe emocje okazało się żądanie zapłaty 60 mln złotych, jakie przedstawiciel Andrzeja Sapkowskiego wystosował do firmy CD Projekt, odpowiedzialnej za interaktywne adaptacje Wiedźmina. Przez ostatnie tygodnie mogliśmy usłyszeć wiele komentarzy na ten temat, ale wszystkie je przebił pisarz Dmitrij Głuchowski, czyli autor serii Metro. Sytuacja obu pisarzy jest w wielu kwestiach podobna. Powieść Metro 2033 zainspirowała serię gier studia 4A Games, a ich popularność przełożyła się na wzrost sprzedaży oryginalnej książki i kolejnych osadzonych w tym uniwersum. Nic zatem dziwnego, że, w wywiadzie z rosyjskim serwisem DTF, twórca został zapytany o tę kwestię. Dmitrij Głuchowski odpowiedział, że Andrzej Sapkowski jest „starym głupcem”, który zawalił sprawę z umową licencyjną i może winić tylko siebie samego. Dodał, że uważa go za „niewdzięcznego palanta”, gdyż bez popularności gier CD Projektu serwis Netflix nigdy nie kupiłby od polskiego pisarza praw do opracowania serialowej adaptacji.\r\nŹródło: https://www.gry-online.pl/S013.asp?ID=111774'),
(14, 'Fallout 76 - testujemy betę na PC [AKT.: ostatnia szansa na grę przed premierą]', 'Aktualizacja, 3 listopada 2018: Przypominamy, że dziś od 22:00 do 2:00 w nocy trwa przedostania beta, a jutro od 19:00 naszego czasu również do 2:00 w nocy, trwać będzie ostatnie spotkanie z betą Fallouta 76. Czy na sali jest ktoś zdecydowany na zakup lub osoby które zamówiły już grę w przedsprzedaży?');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id_u` int(11) NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(100) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `artykuly`
--
ALTER TABLE `artykuly`
  ADD PRIMARY KEY (`id_a`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_u`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `artykuly`
--
ALTER TABLE `artykuly`
  MODIFY `id_a` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id_u` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
