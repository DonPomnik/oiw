﻿<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<script src = "scripts/refreshing.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<title> Blog </title>
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer;z-index: 99;" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php" ><img src = "images/logo.jpg"  class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">	
		<hr>
		<div class='alert alert-secondary'>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>

	<?php  if (isset($_SESSION['username'])) : ?>
    	<p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" class="btn btn-danger">logout</a> </p>
    <?php endif ?>
	</div>
<hr>
	 <div  ng-app="myApp" ng-controller="myCtrl">
		<button ng-click="myFunc()" class="btn btn-info" >Filtry</button>
        <div ng-show="showMe" class="filtry">
		<form action ="filters.php" method ="post">
			<label for="head">Nagłówek</label>
			<input name="head" type="text" placeholder="wyszykaj nagłówka">
			<label for="dating">data dodania</label>
			<input name = "dating" type="date">
			<input type ="submit" value="wyszukaj">
		</form>
		</div>
	</div>
			<div id="abc">
			<center>
			<div id="overlay" onclick="off()">
			<div id="text">Tadaaaa :D</div></div>
			<button onclick="on()">Kliknij mnie</button>
				<button id="myBtn">Open Modal</button>

				<div id="myModal" class="modal">

					<div class="modal-content">
						<span class="close">&times;</span>
						<p>Strona jest w fazie beta-testów. Prosimy o wyrozumiałość :).</p>
					</div>

				</div>
			<br><br><br><br>
				<button class="accordion">Protagonist</button>
				<div class="panel">
					<div class="container">
						<img src="images/protagonist.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Protagonista</div>
							</div>
					</div>
				<p>The protagonist of Persona 5 is a transfer student at Shujin Academy. At the beginning of the game, he has little to no power or influence in the world and is restrained by the rules of the system he was born into.

However, beneath his quiet demeanor is a strong-willed juvenile delinquent known throughout the world as The Phantom, to the residents of the Velvet Room he is known as the Trickster, and to his teammates his code name is Joker. The protagonist is the leader of the rebellious Phantom Thieves of Hearts which aims to change society by touching the hearts of people and performing illegal heists.</p>
				</div>
				
				<button class="accordion">Ryuji</button>
				<div class="panel">
				<div class="container">
						<img src="images/Ryuji.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Ryuji</div>
							</div>
					</div>
				<p>Ryuji Sakamoto (坂本 竜司 Sakamoto Ryūji), using the Phantom Thief codename "Skull", is the protagonist's first companion. Hashino describes Ryuji as "defiant", but a "nice guy." Offended by the slightest comments from others, he can sometimes resort to violence. He came from a family with an abusive father, which caused him and his mother to move away.

Ryuji was formerly the star runner on Shujin Academy's track and field team. He was forced to quit after Kamoshida broke one of his legs when he stood up to him. Because Ryuji struck Kamoshida, the track team was suspended; he was blamed for the incident, earning the reputation of a delinquent at school.

Ryuji's persona is Captain Kidd, based on the Scottish pirate William Kidd. In battle, he fights with clubs and shotguns. As a confidant, Ryuji represents the Chariot Arcana and allows the Phantom Thieves to defeat lower-level Shadows. The protagonist helps Ryuji rediscover his love for running and re-establish the track team. Despite reuniting the team, Ryuji plans to run on his own instead. Upon completing his Confidant, Ryuji's Persona takes the form of Seiten Taisei, who is known for his immense strength. In gratitude, he gives the protagonist a sports watch when he leaves Tokyo. Ryuji is voiced by Mamoru Miyano in Japanese and Max Mittelman in English.</p>
				</div>
				
				<button class="accordion">Morgana</button>
				<div class="panel">
				<div class="container">
						<img src="images/morgana.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Morgana</div>
							</div>
					</div>
				<p>Morgana (モルガナ Morugana), using the Phantom Thief code name "Mona", is a cat found by the protagonist and Ryuji in Kamoshida's Palace. Due to his extensive knowledge of the Metaverse, Morgana is their guide before Futaba joins the Phantom Thieves. Hashino describes Morgana as similar to Persona 4's Teddie and a mentor of the Phantom Thieves. Outside the Metaverse, Morgana is a domestic cat and lives with the protagonist. He comments on the protagonist's actions. Morgana is infatuated with Ann (whom he calls "Lady Ann" (杏殿 An-dono)), and argues with Ryuji.

His persona is Zorro, which Hashino describes as Morgana's ideal human form. Morgana fights with a slingshot and curved sword. He can transform into a minivan in the Metaverse, which allows the Phantom Thieves to travel quickly. Morgana represents the Magician Arcana as a Confidant, allowing the protagonist to craft a variety of tools to help the Phantom Thieves explore the Metaverse. He seeks the protagonist's help to discover his true form, and believes that he was once a human (despite his appearance). After the Phantom Thieves enter further into Mementos, Morgana learns that he was created by Igor in the Velvet Room to help guide the protagonist against Yaldabaoth. After Yaldabaoth is defeated, Morgana loses his Metaverse form and vows to remain with the protagonist in the hope of becoming human. Completing his Confidant, Morgana's Persona evolves into Mercurius. He gives his scarf to the protagonist when they leave Tokyo. Morgana is voiced by Ikue Ōtani in Japanese and Cassandra Lee Morris in English.</p>
				</div>
				
				<button class="accordion">Ann</button>
				<div class="panel">
				<div class="container">
						<img src="images/ann.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Ann</div>
							</div>
					</div>
				<p>Ann Takamaki (高巻 杏 Takamaki An, Anne in the Japanese version), using the Phantom Thief codename "Panther", is a one-quarter-American girl in the protagonist's class who recently returned to Japan. She speaks English fluently, and is a model. Despite her popularity and unique appearance, Ann is shunned by the girls in her class because of her rumored relationship with the gym teacher Kamoshida. Hashino describes Ann as "the life of the party", who "will have an impact on the fate of the main characters."

When Ann discovers that her best friend Shiho Suzui's suicide attempt is linked to Kamoshida, she joins the Phantom Thieves. Her Persona is Carmen, whom Hashino describes as a "femme fatale character." Ann fights with whips and submachine guns. She represents the Lovers Arcana as a Confidant, and helps the protagonist negotiate with the Shadows. As the protagonist befriends her, Ann improves herself for Shiho and begins taking her modeling career seriously.

When she completes her Confidant, Ann's Persona evolves into Hecate. In gratitude, she gives the protagonist a fashion magazine when he leaves Tokyo. If the protagonist is in a relationship with her, she gives him earmuffs for Christmas. Ann is voiced by Nana Mizuki in Japanese and Erika Harlacher in English.</p>
				</div>
				
				<button class="accordion">Yusuke</button>
				<div class="panel">
				<div class="container">
						<img src="images/protagonist.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Protagonista</div>
							</div>
					</div>
				<p>Yusuke Kitagawa (喜多川 祐介 Kitagawa Yūsuke), using the Phantom Thief codename "Fox", is a student in the fine-arts department of Kosei High School. A composed, elegant, observant young man, he has a passion for art and is concerned with the aesthetics of his environment. Yusuke has been sheltered throughout his life, and is eccentric around other people. Because he is poor, he often skips meals to save money for art supplies.

Yusuke's mother died when he was three, leaving him an orphan. He was taken in by his mentor, the painter Ichiryusai Madarame. Yusuke sees him as a father figure and idolizes him for creating his favorite painting, Sayuri. However, he learns that Sayuri was a self-portrait by his mother, and that Madarame allowed her to die to steal the credit for it and painted over Yusuke to decontextualize her expression, manufacturing mystery for the sake of artistic appeal.

Yusuke's Persona is Goemon, an outlaw based on Ishikawa Goemon. He fights with Japanese swords and assault rifles. As a Confidant, Yusuke represents the Emperor Arcana and assists the Phantom Thieves by copying skill cards. After Madarame's change of heart, Yusuke has difficulty regaining his ability to create art. The protagonist helps Yusuke to regain his ability to paint, rediscover his love for art, and see the beauty of the world around him; this inspires him to paint Desire and Hope.

When he completes his Confidant, Yusuke's Persona evolves into Kamu Susano-o. In gratitude, he gives the protagonist Desire and Hope when he leaves Tokyo. Yusuke is voiced by Tomokazu Sugita in Japanese and Matthew Mercer in English.</p>
				</div>
				
				<button class="accordion">Makoto</button>
				<div class="panel">
				<div class="container">
						<img src="images/makoto.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Makoto</div>
							</div>
					</div>
				<p>Makoto Niijima (新島 真 Niijima Makoto), using the Phantom Thief codename "Queen", is the student-council president at Shujin Academy. Makoto, the school's top student, is considered the brains of the Phantom Thieves. Analytical, she can deduce conclusions, theorize in several situations, and make elaborate plans to succeed. Behind this exterior, she has an inferiority complex and often praises her sister Sae Niijima. Makoto feels that she is a burden to Sae because of their father's death, which is her motivation to work hard, be successful, and reduce her sister's stress.

Makoto starts tailing the Phantom Thieves to find information for Principal Kobayakawa. Learning their identities, she tasks the team with changing the heart of mafia boss Junya Kaneshiro. Makoto's persona is Johanna (based on Pope Joan), and she fights with revolvers and tekko.

As a Confidant, Makoto represents the Priestess Arcana and provides the Phantom Thieves with a more-detailed analysis of in-game enemies. As the protagonist gets closer to her, Makoto decides to become better-acquainted with her classmates and befriends Eiko Takao. She discovers that Eiko is in a relationship with Tsukasa, a shifty-looking man who has manipulated girls into working for illegal businesses in Shinjuku. Makoto reveals Tsukasa's true nature to Eiko with the protagonist's help, and decides to follow her father onto the police force.

When she completes her Confidant, Makoto's Persona evolves into Anat. In gratitude, she gives the protagonist a "Buchi" calculator when he leaves Tokyo. If the protagonist is in a relationship with her, she gives him a wristwatch for Christmas. Makoto is voiced by Rina Satō in Japanese and Cherami Leigh in English.</p>
				</div>
				
				
			</center>
			</div>
			<button onclick="funkcjaPokazowa()">Futaba</button>

			<div id="myDIV">
			<div class="container">
						<img src="images/futaba.jpg" alt="Avatar" class="image" style="width:100%">
							<div class="middle">
								<div class="text">Futaba</div>
							</div>
					</div>
				<p>Futaba Sakura (佐倉 双葉 Sakura Futaba), using the Phantom Thief code name "Oracle" ("Navi" in the Japanese version), is the adopted daughter of Sojiro Sakura (the owner of the cafe where the protagonist lives). Although Futaba is a genius computer hacker, she is a recluse at the beginning of the game and fearful of large crowds. Shortly before the game begins, she sees her mother (Wakaba Isshiki) killed by a car and is framed for her death with a forged suicide note. Futaba is bounced among abusive family members until Sojiro intervenes and took her in. She has auditory hallucinations of her extended family's reactions to the suicide note, and locks herself in her bedroom. Futaba appears withdrawn in the game, and has developed a Palace depicting her sloth in the form of a pyramid. She has become so depressed that her Shadow represents her repressed positive emotions and helps the Phantom Thieves traverse her Palace. As the Phantom Thieves slowly become more popular, Futaba blackmails them into changing her heart with the alias "Alibaba". After entering her Palace and discovering the truth about her mother's death, Futaba awakens her Persona (Necronomicon) and helps the Phantom Thieves defeat a sphinx representing her mother's perceived wrath towards her. She later replaces Morgana as the party's navigator.

As a Confidant, Futaba represents the Hermit Arcana and provides detailed locations and extra assistance in battle. After learning to adjust to a normal life, she makes a "promise list" of goals she wants to reach with the help of the protagonist. As Futaba slowly recovers, she reveals that in her youth she was bullied for her intelligence and had only one friend (Kana). Futaba discovers that Kana's parents were abusing her. She tracks down Kana, and learns that the abuse has continued. After changing her parents' hearts, Kana and Futaba rebuild their friendship.

When she completes her Confidant, Futaba's Persona evolves into Prometheus. In gratitude, she gives the protagonist her promise list when he leaves Tokyo since she no longer needs it. If the protagonist is in a relationship with her, she gives him headphones for Christmas. Futaba is voiced by Aoi Yūki in Japanese and Erica Lindbeck in English.</p>
			</div>
		</main>
		<footer>
		</footer>
		<script src="scripts/slider.js"></script>
	<script>
	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.display === "block") {
				panel.style.display = "none";
			} else {
				panel.style.display = "block";
			}
		});
	}
	
	// Get the modal
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	btn.onclick = function() {
		modal.style.display = "block";
	}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
	
	function funkcjaPokazowa() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function on() {
  document.getElementById("overlay").style.display = "block";
}

function off() {
  document.getElementById("overlay").style.display = "none";
}
	</script>
	</body>
</html>