Ryuji Sakamoto (?? ?? Sakamoto Ryuji), using the Phantom Thief codename "Skull", is the protagonist's first companion. Hashino describes Ryuji as "defiant", but a "nice guy." Offended by the slightest comments from others, he can sometimes resort to violence. He came from a family with an abusive father, which caused him and his mother to move away.

Ryuji was formerly the star runner on Shujin Academy's track and field team. He was forced to quit after Kamoshida broke one of his legs when he stood up to him. Because Ryuji struck Kamoshida, the track team was suspended; he was blamed for the incident, earning the reputation of a delinquent at school.

Ryuji's persona is Captain Kidd, based on the Scottish pirate William Kidd. In battle, he fights with clubs and shotguns. As a confidant, Ryuji represents the Chariot Arcana and allows the Phantom Thieves to defeat lower-level Shadows. The protagonist helps Ryuji rediscover his love for running and re-establish the track team. Despite reuniting the team, Ryuji plans to run on his own instead. Upon completing his Confidant, Ryuji's Persona takes the form of Seiten Taisei, who is known for his immense strength. In gratitude, he gives the protagonist a sports watch when he leaves Tokyo. Ryuji is voiced by Mamoru Miyano in Japanese and Max Mittelman in English.