﻿<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<script src = "scripts/refreshing.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<title> Blog </title>
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer;z-index: 99;" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php" ><img src = "images/logo.jpg"  class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">	
		<hr>
		<div class='alert alert-secondary'>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>

	<?php  if (isset($_SESSION['username'])) : ?>
    	<p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" class="btn btn-danger">logout</a> </p>
    <?php endif ?>
	</div>
<hr>
	 <div  ng-app="myApp" ng-controller="myCtrl">
		<button ng-click="myFunc()" class="btn btn-info" >Filtry</button>
        <div ng-show="showMe" class="filtry">
		<form action ="filters.php" method ="post">
			<label for="head">Nagłówek</label>
			<input name="head" type="text" placeholder="wyszykaj nagłówka">
			<label for="dating">data dodania</label>
			<input name = "dating" type="date">
			<input type ="submit" value="wyszukaj">
		</form>
		</div>
	</div>
			<div id="abc">
			<center>
				<?php
					include('scripts/show.php');
				?>
			</center>
			</div>
		</main>
		<footer>
		</footer>
		<script src="scripts/slider.js"></script>
		
	</body>
</html>